exim -bp | grep \< | awk '{print $3}' | xargs exim -Mrm
for user in `/bin/ls -A /var/cpanel/users` ; do rm -fv /home/$user/backup-*$user.tar.gz ; done
find /home*/*/tmp/ -type f -name "Cpanel_*" -print -delete
find /var/log -name "*.gz" -print -delete
find /var/log/server-status/ -mtime +1 -type d -exec rm -Rf {} \;
rm -fv /var/log/exim_mainlog-*
rm -fv /var/log/exim_paniclog-*
rm -fv /var/log/exim_rejectlog-*
rm -fv /var/log/lve-stats.log-*
find /home*/ -type f -name "*.tar.gz" -print -delete
rm -rfv /home*/*/softaculous_backups
rm -rfv /home*/*/public_html/wp-content/updraft/*
rm -rfv /home*/*/.trash/*
find /home*/ -type f -name "*.wpress" -print -delete
find /home*/ -type f -name error_log -print -delete
find /home*/ -type f -name "*.rar" -print -delete
find /home*/ -type f -name "*.iso" -print -delete
find /home*/ -type f -name "*.exe" -print -delete
find /home*/ -type f -name "*.xen" -print -delete
find /home*/ -type f -name "*.7z" -print -delete
find /home*/ -type f -name "*.tar" -print -delete
find /home*/ -type f -name "*.tar.gz" -print -delete
#find /home*/ -type f -name "*.jpa" -print -delete
#find /home*/ -type f -name "*.mp4" -print -delete
#find /home*/ -type f -name "*.mp3" -print -delete
find /home*/ -type f -name "*.wav" -print -delete
find /home*/*/ -type f -name error_log -print -delete
rm -rfv /home*/*/backupbuddy_backups/*
rm -rfv /home*/*/com_akeeba/backup/*
#find /home*/ -type f -name "*backup_*" -print -delete # Removed - 	#BRX-5238556
find /home*/*/ -type f -name "*_backup" -print -delete

MAILDIRS=$(find /home/*/mail/*/* -maxdepth 0 -type d)
INBOXFOLDERS=(.Trash .Junk .Spam .Low\ Priority .cPanel\ Reports)
for basedir in $MAILDIRS; do
for ((i = 0; i < ${#INBOXFOLDERS[*]}; i++)); do
for dir in cur new; do
[ -e “$basedir/${INBOXFOLDERS[$i]}/$dir” ] && (
echo “Processing $basedir/${INBOXFOLDERS[$i]}/$dir…”
find “$basedir/${INBOXFOLDERS[$i]}/$dir/” -type f -mtime +30 -delete
)
done
done
done
/scripts/generate_maildirsize -–allaccounts --confirm --verbose