#!/bin/bash

# bash <(curl -s https://gitlab.com/brixly/file-dump/raw/master/scripts/set_max_senders.sh)


# Correct Account-specific Max hourly emails per domain settings
grep -rl "MAX_EMAIL_PER_HOUR=" /var/cpanel/users | xargs sed -i "s/MAX_EMAIL_PER_HOUR=.*/MAX_EMAIL_PER_HOUR=100/g"
grep -rl "MAX_DEFER_FAIL_PERCENTAGE=" /var/cpanel/users | xargs sed -i "s/MAX_DEFER_FAIL_PERCENTAGE=.*/MAX_DEFER_FAIL_PERCENTAGE=125/g"

/usr/local/cpanel/scripts/updateuserdomains

# Set Tweak Setting Values
whmapi1 set_tweaksetting key=maxemailsperhour value=100
whmapi1 set_tweaksetting key=email_send_limits_defer_cutoff value=125
whmapi1 set_tweaksetting key=email_outbound_spam_detect_action value=block
whmapi1 set_tweaksetting key=email_send_limits_count_mailman value=1
whmapi1 set_tweaksetting key=nobodyspam value=1
whmapi1 set_tweaksetting key=skipspamassassin value=0
whmapi1 set_tweaksetting key=email_send_limits_min_defer_fail_to_trigger_protection value=15

sed -i "s/: .*/: 100,60,15/g" /etc/email_send_limits

# Create Cronjob
wget -O /scripts/set_max_senders.sh https://gitlab.com/brixly/file-dump/raw/master/scripts/set_max_senders.sh
echo "0 */12 * * * /bin/bash /scripts/set_max_senders.sh > /dev/null 2>&1" > /etc/cron.d/set_max_senders