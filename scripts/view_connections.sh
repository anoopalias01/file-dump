printf "\nCurrent Load / Uptime:\n"
w

printf "\nConnections by Server / Destination IP:\n"
netstat -alpn | grep :80 | awk '{print $4}' |awk -F: '{print $(NF-1)}' |sort | uniq -c | sort -n

printf "\nConnections Breakdown:"
netstat -tn|awk '{print $6}'|sort|uniq -c

printf "\nConnections by External IP:\n"
netstat -ntu | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n | tail

printf "\nApache Domlogs by Size:\n"
ls -lathS /etc/apache2/logs/domlogs/ | head -5

printf "\nLVE Stats / 24h:\n"
lveinfo -d --period=1d --by-fault=any --show-columns="ID,CPUf,EPf,PMemF,NprocF,IOf,IOPSf"

printf "\nLVE Stats for past 60 minutes - EP (See EPf Column):\n"
lveinfo --by-fault=ep --period=1h -d --limit=5

printf "\nLVE Stats for past 60 minutes - Memory Usage (See PMemF Column):\n"
lveinfo --by-fault=pmem --period=1h -d --limit=5

printf "\n"
read -p "Do you want to check Apache status via lynx? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    lynx http://localhost/whm-server-status
fi

printf "\n"
read -p "Do you want to check Nginx status via lynx? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    lynx http://localhost/nginx_status
fi

printf "\n"
read -p "Do you want to check running MySQL processes? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mysqladmin pr
fi

printf "\n"
read -p "Do you want to tail the logs? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    multitail /etc/apache2/logs/error_log /var/log/nginx/error_log /var/log/lfd.log /var/log/messages
fi
