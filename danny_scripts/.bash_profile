# .bash_profile
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
      . ~/.bashrc
fi
# User specific environment and startup programs
alias nameservers="cat /var/lib/nameservers.txt"
PATH=$PATH:$HOME/bin
export PATH